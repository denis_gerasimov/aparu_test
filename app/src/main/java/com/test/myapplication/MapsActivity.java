package com.test.myapplication;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, TrackingUIUpdates {

    View map1Button;
    MapsActivity activity;
    Marker marker;
    private GoogleMap googleMap;
    private GPSTracker tracker;
    private Boolean isTrackingEnabled = false;
    private PolylineOptions polylineOptions = null;
    String currentTrackName = null;
    String trip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        Calendar cal = Calendar.getInstance();
        Date date=cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        trip = getIntent().getStringExtra("TripID");


        setContentView(R.layout.activity_maps);
        ((MapFragment) getFragmentManager().findFragmentById(R.id.mapView)).getMapAsync(this);
        map1Button = findViewById(R.id.map1_button);
        tracker = new GPSTracker(this, this);
        View back = (View) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        View start = (View) findViewById(R.id.map_start_button);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isTrackingEnabled = true;
                Tracks.getInstance(activity).addTrack(currentTrackName);
                addPolyline(Tracks.getInstance(activity).getTrackByName(currentTrackName));
            }
        });
        View pause = (View) findViewById(R.id.map_pause_button);
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isTrackingEnabled = false;
            }
        });
        currentTrackName = dateFormat.format(date);
    }

    private void addPolyline(List<LatLng> points){
        polylineOptions = new PolylineOptions();
        polylineOptions.color(Color.GREEN);
        polylineOptions.addAll(points);
        googleMap.addPolyline(polylineOptions);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        setUserMarker();
        googleMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(new LatLng(tracker.getLatitude(), tracker.getLongitude()), 14));
        map1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUserMarker();
                googleMap.moveCamera(CameraUpdateFactory
                        .newLatLngZoom(new LatLng(tracker.getLatitude(), tracker.getLongitude()), 14));
            }
        });

        if (trip != null) {
            currentTrackName = trip;
            for (Map.Entry<String, List<LatLng>> entry : Tracks.getInstance(this).getTracks().entrySet()) {
                String key = entry.getKey();
                setTrack(Tracks.getInstance(activity).getTrackByName(key));
            }
        }
    }

    private void setUserMarker(){
        if (marker == null){
            MarkerOptions clientMarker = new MarkerOptions();
            clientMarker.draggable(true);
            clientMarker.position(new LatLng(tracker.getLatitude(), tracker.getLongitude()));
            marker = googleMap.addMarker(clientMarker);
        }
        else {
            marker.setPosition(new LatLng(tracker.getLatitude(), tracker.getLongitude()));
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        tracker.stopUsingGPS();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void setTrack(List<LatLng> points){
        addPolyline(points);
    }

    @Override
    public void onUpdate() {
        setUserMarker();
        if (isTrackingEnabled){
            Tracks.getInstance(this).addPointInTrack(new LatLng(tracker.getLatitude(), tracker.getLongitude()), currentTrackName);
            setTrack(Tracks.getInstance(this).getTrackByName(currentTrackName));
        }
    }
}
