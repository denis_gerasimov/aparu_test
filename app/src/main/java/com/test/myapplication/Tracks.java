package com.test.myapplication;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class Tracks {
    private static volatile Tracks instance;
    private static final String FILENAME = "MY_TRACKS";
    private LinkedHashMap<String, List<LatLng>> tracks = new LinkedHashMap<String, List<LatLng>>();

    public List<LatLng> getTrackByName(String name){
        return tracks.get(name);
    }

    public LinkedHashMap<String, List<LatLng>> getTracks(){
        return tracks;
    }

    public void addPointInTrack(LatLng point, String trackName){
        if (!tracks.containsKey(trackName)){
            addTrack(trackName);
        }
        tracks.get(trackName).add(point);
    }

    public void addTrack(String name){
        tracks.put(name, new ArrayList<LatLng>());
    }

    private Tracks(Context context) {
        instance = this;
        tracks = new LinkedHashMap<String, List<LatLng>>();
    }

    public static Tracks getInstance(Context context) {
        Tracks localInstance = instance;
        if (localInstance == null) {
            synchronized (Tracks.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Tracks(context);
                }
            }
        }
        return localInstance;
    }
}
