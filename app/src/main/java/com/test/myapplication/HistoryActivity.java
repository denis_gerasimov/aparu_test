package com.test.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Map;

public class HistoryActivity extends AppCompatActivity {
    LinearLayout mainLayout;
    HistoryActivity activity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = LayoutInflater.from(this).inflate(R.layout.activity_history, null);
        setContentView(rootView);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        View back = (View) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mainLayout = rootView.findViewById(R.id.ll);
        for (Map.Entry<String, List<LatLng>> entry : Tracks.getInstance(this).getTracks().entrySet()) {
            final String key = entry.getKey();

            View v = LayoutInflater.from(this).inflate(R.layout.cell, null);
            TextView title = v.findViewById(R.id.time_text);
            title.setText(key);
            mainLayout.addView(v);
            ImageView divider = new ImageView(this);
            divider.setImageResource(R.drawable.divider);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, MapsActivity.class);
                    intent.putExtra("TripID", key);
                    startActivity(intent);
                }
            });
            mainLayout.addView(divider);
        }
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
